<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (Exception $exception) {
            return $this->handleException($exception);
        });

        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function handleException(Exception $exception)
    {
        $data['success'] = false;
        $data["message"] = "";

        if ($exception instanceof ValidationException) {
            $errors = $exception->errors();
            foreach ($errors as $error) {
                $data['message'] = $error[0];
            }
        } else {
//            dd($exception);
            $data['message'] = "Invalid request , can't find requested data 5yh ";
            $data['exception-message'] = $exception->getMessage();
        }

        return response($data);
    }
}
