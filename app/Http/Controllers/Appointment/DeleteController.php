<?php

namespace App\Http\Controllers\Appointment;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeleteAppointmentRequest;
use App\Models\Appointment;

class DeleteController extends Controller
{
    public function index(DeleteAppointmentRequest $request)
    {
        $data = $request->validated();
        $item = Appointment::find($data['appointment_id']);
        $item->delete();
        return $this->response();
    }
}
