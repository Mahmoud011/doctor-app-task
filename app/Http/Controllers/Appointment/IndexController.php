<?php

namespace App\Http\Controllers\Appointment;

use App\Http\Controllers\Controller;
use App\Http\Resources\Appointment\ListResource;
use App\Models\Appointment;
use App\ModulesConst\Paginate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function index(Request $request)
    {
        $items = Appointment::where("patient_id", Auth::id());
        $items = $this->filter($items, $request);
        return $this->response(ListResource::collection($items));
    }

    private function filter($item, $request)
    {
        if ($request->appointmentType) {
            $now = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
            if ($request->appointmentType == 1) {
                // need the futher items ,
                $items = $item->where('start_date', '>', $now);
            } else {
                $items = $item->where('end_date', '>', $now);
            }
        }
        return $item->orderBy("id", "Desc")->paginate(Paginate::value);
    }
}
