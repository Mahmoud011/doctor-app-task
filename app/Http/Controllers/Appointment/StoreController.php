<?php

namespace App\Http\Controllers\Appointment;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAppointmentRequest;
use App\Models\Appointment;
use Illuminate\Support\Facades\Auth;

class StoreController extends Controller
{
    public function index(StoreAppointmentRequest $request)
    {
        $data = $request->validated();
        $data["patient_id"] = Auth::id();
        Appointment::create($data);
        return $this->response();
    }
}
