<?php

namespace App\Http\Controllers\Appointment;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateAppointmentRequest;
use App\Http\Resources\Appointment\ListResource;
use App\Models\Appointment;

class updateController extends Controller
{
    public function index(UpdateAppointmentRequest $request)
    {
        $data = $request->validated();
        $item = Appointment::find($data['appointment_id']);
        $item->update($data);
        return $this->response(ListResource::make($item));
    }
}
