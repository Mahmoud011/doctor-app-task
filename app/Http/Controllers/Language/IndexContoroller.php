<?php

namespace App\Http\Controllers\Language;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;

class IndexContoroller extends Controller
{
    public function index()
    {
        $array = Lang::get('lang'); // return entire array
        return $array;
    }
}
