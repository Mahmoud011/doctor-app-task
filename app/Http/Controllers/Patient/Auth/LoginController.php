<?php

namespace App\Http\Controllers\Patient\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\PatientLoginRequest;
use App\Http\Resources\Patient\ProfileResource;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index(PatientLoginRequest $request)
    {
        $data = $request->validated();
        $user = auth()->attempt(["email" => $data["email"], "password" => $data["password"]]);
        if (!$user)
            return $this->response([], false, trans("auth.failed"));

        $user = Auth::user();
        $token = Auth::user()->createToken('appToken');
        $accessToken = $token->accessToken;
        return $this->sendResponse(ProfileResource::make($user), true, trans("auth.success"), $accessToken);
    }
}
