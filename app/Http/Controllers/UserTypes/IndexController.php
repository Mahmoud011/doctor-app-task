<?php

namespace App\Http\Controllers\UserTypes;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserType\ListResource;
use App\Models\UserType;
use App\ModulesConst\Paginate;

class IndexController extends Controller
{
    public function index()
    {
        $data = UserType::paginate(Paginate::value);
        return $this->response(ListResource::collection($data));
    }
}
