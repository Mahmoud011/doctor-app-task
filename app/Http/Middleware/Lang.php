<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Lang
{
    public function handle(Request $request, Closure $next)
    {
        $languages = ["ar", "en"];
        $defaultLanguage = "en";
        if ($request->header('lang') && in_array($request->header('lang'), $languages))
            app()->setLocale($request->header('lang'));
        else
            app()->setLocale($defaultLanguage);
        return $next($request);
    }
}
