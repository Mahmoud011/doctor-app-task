<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeleteAppointmentRequest extends FormRequest
{
    public function rules()
    {
        return [
            'appointment_id' => "required"
        ];
    }
}
