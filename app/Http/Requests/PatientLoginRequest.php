<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatientLoginRequest extends FormRequest
{

    public function rules()
    {
        return [
            "email" => 'required',
            "password" => 'required'
        ];
    }
}
