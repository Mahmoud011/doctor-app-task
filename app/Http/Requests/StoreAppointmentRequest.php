<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAppointmentRequest extends FormRequest
{
    public function rules()
    {
        return [
            'doctor_id' => "required",
            'start_date' => "required",
            'end_date' => "required",
        ];
    }
}
