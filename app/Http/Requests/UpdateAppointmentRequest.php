<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAppointmentRequest extends FormRequest
{
    public function rules()
    {
        return [
            'appointment_id' => "required",
            'doctor_id' => "required",
            'start_date' => "required",
            'end_date' => "required",
            'status' => "",
        ];
    }
}
