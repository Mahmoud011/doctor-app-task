<?php

namespace App\Http\Resources\Appointment;

use Illuminate\Http\Resources\Json\JsonResource;

class ListResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "doctorName" => $this->doctor->name,
            "patientName" => $this->patient->name,
            "startDate" => $this->start_date,
            "endDate" => $this->end_date,
            "status" => $this->status_name,
        ];
    }
}
