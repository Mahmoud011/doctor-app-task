<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;

    protected $fillable = [
        "doctor_id",
        "patient_id",
        "start_date",
        "end_date",
        "status",
    ];

    public function doctor()
    {
        return $this->belongsTo(User::class, "doctor_id");
    }

    public function patient()
    {
        return $this->belongsTo(User::class, "patient_id");
    }

    public function getStatusNameAttribute()
    {
        if ($this->status == "pending") {
            return trans("lang.pending");
        } elseif ($this->status == "completed") {
            return trans("lang.completed");
        } elseif ($this->status = "canceled") {
            return trans("lang.canceled");
        } else {
            return trans(".not_selected");
        }
    }
}
