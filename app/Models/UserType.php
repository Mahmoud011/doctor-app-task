<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        "name_ar", "name_en"
    ];

    public function getNameAttribute()
    {
        return $this->{'name_' . app()->getLocale()};
    }

}
