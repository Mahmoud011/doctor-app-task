<?php

namespace App\Traits;

trait ResponseHandler
{
    public function response($data = null, $status = true, $message = null)
    {
        $response['message'] = $message ? $message : trans("lang.success_msg");
        $response['success'] = $status;
        if ($data)
            $response['data'] = $data;
        return $response;
    }

    public function sendResponse($data = null, $status = true, $message = null, $accessToken = null)
    {
        $response['message'] = $message ? $message : trans("lang.success_msg");
        $response['success'] = $status;
        if ($data)
            $response['data'] = $data;
        $response['accessToken'] = $accessToken;
        $response['tokenType'] = "Bearer";
        return $response;
    }
}
