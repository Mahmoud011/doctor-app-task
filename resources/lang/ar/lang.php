<?php

return [

    'pending' => 'قيد الانتظار',
    'completed' => 'اكتمل',
    'canceled' => 'ملغي',
    'not_selected' => 'غير محدد',
    'success_msg' => 'تم تنفيذ الامر بنجاح',

];
