<?php

return [

    'pending' => 'Pending',
    'completed' => 'Completed',
    'canceled' => 'Canceled',
    'not_selected' => 'Not selected',
    'success_msg' => 'The process was executed successfully',
];
