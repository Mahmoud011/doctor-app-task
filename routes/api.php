<?php

use Illuminate\Support\Facades\Route;

Route::post('userTypes', [\App\Http\Controllers\UserTypes\IndexController::class, 'index']);
Route::post('login', [\App\Http\Controllers\Patient\Auth\LoginController::class, 'index']);
Route::post('languages', [\App\Http\Controllers\Language\IndexContoroller::class, 'index']);
