<?php

use Illuminate\Support\Facades\Route;

// * Appointments Crud *
Route::post('appointments', [\App\Http\Controllers\Appointment\IndexController::class, 'index']);
Route::post('appointment/store', [\App\Http\Controllers\Appointment\StoreController::class, 'index']);
Route::post('appointment/update', [\App\Http\Controllers\Appointment\updateController::class, 'index']);
Route::post('appointment/delete', [\App\Http\Controllers\Appointment\DeleteController::class, 'index']);
